
var ImageCounter = function(props) {
	return (
		<div>
			<div className="count">{props.count}</div>
			<img src={'img/' + props.imageUrl} alt={props.title} onClick={props.onCount} />
		</div>
		);
};

var Hero = createReactClass({
	getInitialState: function() {
		return {
			count: 0
		};
	},
	clickHandler: function() {
		this.setState({count: this.state.count + 1});
	},
	render: function() {
		return (
			<div className="container" onClick={this.clickHandler}>
				<h1>{this.props.title}</h1>
				<ImageCounter 
					imageUrl={this.props.imageUrl} 
					count={this.state.count} 
					onCount={this.clickHandler} />
				<a href={this.props.link}></a>
			</div>);
	}
});

var App = createReactClass({
	render: function() {
		return (
			<div className="wrapper">
				{this.props.heroes.map(function(hero) {
					return <Hero 
								key={hero.id} 
								title={hero.title} 
								imageUrl={hero.imageUrl} 
								link={hero.link} />;
				})}
			</div>);
	}
});

var data = [
	{
		id: 1,
		title: 'React',
		imageUrl: 'react.png',
		link: 'https://reactjs.org'
	},{
		id: 2,
		title: 'Angular 2',
		imageUrl: 'angular.png',
		link: 'https://angular.io'
	},{
		id: 3,
		title: 'Ember',
		imageUrl: 'ember.png',
		link: 'https://www.emberjs.com'
	},{
		id: 4,
		title: 'Vue',
		imageUrl: 'vue.png',
		link: 'https://vuejs.org'
	}
];

ReactDOM.render(<App heroes={data} />, document.getElementById('root'));